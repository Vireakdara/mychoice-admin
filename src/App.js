import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Dashboard from "./components/dashboard/Dashboard";
import UsersManagement from "./components/users_management/UsersManagement";
import University from "./components/university/University";
import Major from "./components/major/Major";
import Subject from "./components/subject/Subject";
import TopRank from "./components/top_rank/TopRank";
import SchoolRequest from "./components/school_request/SchoolRequest";
import SchoolTrainingReport from "./components/reports_info/school_training_report/SchoolTrainingReport";
import ExperienceReport from "./components/reports_info/experience_report/ExperienceReport";
import UniversityReport from "./components/reports_info/university_report/UniversityReport";
import QuizTest from "./components/quiz_test/QuizTest";
import Navbar from "./components/navbar/Navbar";
import Sidebar from "./components/sidebar/Sidebar";


function App() {
  return (
    <div>
      <Router>
        <Navbar/>
        <Switch>
          {/* Dashboard */}
          <Route path="/" exact component={Dashboard} />
          <Route path="/dashboard" component={Dashboard} />

          {/* Users Management */}
          <Route path="/users_management" exact component={UsersManagement} />

          {/* Univerisity */}
          <Route path="/univeristy" exact component={University} />

          {/* Major */}
          <Route path="/major" exact component={Major} />

          {/* Basic Knowledge */}
          <Route path="/basic_knowledge" exact component={Subject} />

          {/* Top Rank */}
          <Route path="/top_rank" exact component={TopRank} />

          {/* School Request */}
          <Route path="/school_request" exact component={SchoolRequest} />

          {/* Reports Info */}
          
          <Route path="/school_training_report" exact component={SchoolTrainingReport} />
          <Route path="/experience_report" exact component={ExperienceReport} />
          <Route path="/university_report" exact component={UniversityReport} />

          {/* Quiz Test */}
          <Route path="/quiz_text" exact component={QuizTest} />
        </Switch>
        <Sidebar/>
      </Router>
    </div>
  );
}

export default App;
