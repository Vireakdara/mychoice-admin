import React, { Component } from "react";
import {Link} from "react-router-dom"
import './Sidebar.css'

export default class Sidebar extends Component {
  render() {
    return (
      <aside className="main-sidebar sidebar-dark-primary elevation-4">
        {/* Brand Logo */}
        <Link to="/dashboard" className="brand-link">
          <img
            src="dist/img/MyChoiceLogoWhiteNew.png"
            alt="AdminLTE Logo"
            className="brand-image img-circle elevation-3"
            style={{ opacity: ".8" }}
          />
          <span className="brand-text font-weight-light">MyChoice</span>
        </Link>
        {/* Sidebar */}
        <div className="sidebar">
          {/* Sidebar user panel (optional) */}
          {/* <div className="user-panel mt-3 pb-3 mb-3 d-flex">
            <div className="image">
              <img
                src="dist/img/user2-160x160.jpg"
                className="img-circle elevation-2"
                alt="User Image"
              />
            </div>
            <div className="info">
              <a href="#" className="d-block">
                Alexander Pierce
              </a>
            </div>
          </div> */}
          {/* Sidebar Menu */}
          <nav className="mt-2">
            <ul
              className="nav nav-pills nav-sidebar flex-column"
              data-widget="treeview"
              role="menu"
              data-accordion="false"
            >
              {/* Add icons to the links using the .nav-icon class
         with font-awesome or any other icon font library */}

              <li className="nav-item">
                <Link to="/dashboard" className="nav-link">
                  <i className="nav-icon fas fa-tachometer-alt" id="icon" />
                  <p className="font">
                    Dashboard
                    {/* <span className="right badge badge-danger">New</span> */}
                  </p>
                </Link>
              </li>

              <li className="nav-item">
                <Link to="/users_management" className="nav-link">
                  <i className="nav-icon fas fa-users" id="icon" />
                  <p>
                   Users Management
                    {/* <span className="right badge badge-danger">New</span> */}
                  </p>
                </Link>
              </li>

              <li className="nav-item">
                <Link to="/univeristy" className="nav-link">
                  <i className="nav-icon fas fa-university" id="icon"/>
                  <p>
                    University
                    {/* <span className="right badge badge-danger">New</span> */}
                  </p>
                </Link>
              </li>

              <li className="nav-item">
                <Link to="/major" className="nav-link">
                  <i className="nav-icon fas fa-book" id="icon"/>
                  <p>
                    Major
                    {/* <span className="right badge badge-danger">New</span> */}
                  </p>
                </Link>
              </li>

              <li className="nav-item">
                <Link to="/basic_knowledge" className="nav-link">
                  <i className="nav-icon fas fa-edit" id="icon"/>
                  <p>
                    Subject
                    {/* <span className="right badge badge-danger">New</span> */}
                  </p>
                </Link>
              </li>

              <li className="nav-item">
                <Link to="/top_rank" className="nav-link">
                  <i className="nav-icon fas fa-medal" id="icon"/>
                  <p>
                    Top Ranking
                    {/* <span className="right badge badge-danger">New</span> */}
                  </p>
                </Link>
              </li>

              <li className="nav-item">
                <Link to="/school_request" className="nav-link">
                  <i className="nav-icon fas fa-envelope-open-text" id="icon"/>
                  <p>
                    School Request
                    {/* <span className="right badge badge-danger">New</span> */}
                  </p>
                </Link>
              </li>


              <li className="nav-item has-treeview">
                <Link to="#" className="nav-link ">
                  <i className="nav-icon fas fa-user-cog" id="icon"/>
                  <p>
                    Reports Info
                    <i className="right fas fa-chevron-left" />
                  </p>
                </Link>
                <ul className="nav nav-treeview">
                  <li className="nav-item">
                    <Link to="/school_training_report" className="nav-link ">
                      <i className="far fa-circle nav-icon" />
                      <p>School Traning Report</p>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/experience_report" className="nav-link">
                      <i className="far fa-circle nav-icon" />
                      <p>Exp Report</p>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/university_report" className="nav-link">
                      <i className="far fa-circle nav-icon" />
                      <p>University Report</p>
                    </Link>
                  </li>
                </ul>
              </li>

              <li className="nav-item">
                <Link to="/quiz_text" className="nav-link">
                  <i className="nav-icon fas fa-clipboard-list" id="icon"/>
                  <p>
                    Quiz Test
                    {/* <span className="right badge badge-danger">New</span> */}
                  </p>
                </Link>
              </li>
            </ul>
          </nav>
          {/* /.sidebar-menu */}
        </div>
        {/* /.sidebar */}
      </aside>
    );
  }
}
