import React, { Component } from "react";
import "./UsersManagement.css";
import { Table, Button, Form } from "react-bootstrap";
import { BsSearch } from "react-icons/bs";

export default class UsersManagement extends Component {
  render() {
    return (
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0 text-dark">Users Management</h1>
              </div>
            </div>
          </div>
        </div>

        <section className="content">
          <div className="container-fluid">
            <div className="row">
              <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 username">
                <label>Username</label>
                <div class="input-group">
                  <input
                    id="msg"
                    type="text"
                    class="form-control"
                    name="msg"
                    placeholder="search any user"
                  />
                  <Button className="bg-purple border-radius-0">
                    <BsSearch />
                  </Button>
                </div>
              </div>
              <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 role">
                <label>Role</label>
                <Form.Control as="select" defaultValue="Choose...">
                  <option>Type...</option>
                  <option>...</option>
                </Form.Control>
              </div>
              <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 gender">
                <label>Gender</label>
                <Form.Control as="select" defaultValue="Choose...">
                  <option>Gender...</option>
                  <option>...</option>
                </Form.Control>
              </div>
            </div>
            <pre></pre>

            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 btn-add-university">
              <Button variant="success">Add + </Button>
              <pre></pre>
            </div>

            <Table striped bordered hover>
              <thead>
                <tr>
                  <th className="th-style">ID</th>
                  <th className="th-style">First Name</th>
                  <th className="th-style">Last Name </th>
                  <th className="th-style">Gender</th>
                  <th className="th-style">Birth Date</th>
                  <th className="th-style">Role </th>
                  <th className="th-style">Created At</th>
                  <th className="th-style">Updated At</th>
                  <th className="th-style">Action</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>VireakDara</td>
                  <td>Ly</td>
                  <td>Male</td>
                  <td>20-20-2020</td>
                  <td>User</td>
                  <td>10-08-2020</td>
                  <td>01-08-2020</td>
                  <td style={{ textAlign: "center" }}>
                    <Button variant="warning" style={{ margin: "5px" }}>
                      Update
                    </Button>
                    <Button variant="danger" style={{ margin: "5px" }}>
                      Delete
                    </Button>
                  </td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Viechka</td>
                  <td>Khat</td>
                  <td>Male</td>
                  <td>20-20-2020</td>
                  <td>Admin</td>
                  <td>10-08-2020</td>
                  <td>01-08-2020</td>
                  <td style={{ textAlign: "center" }}>
                    <Button variant="warning" style={{ margin: "5px" }}>
                      Update
                    </Button>
                    <Button variant="danger" style={{ margin: "5px" }}>
                      Delete
                    </Button>
                  </td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Visal</td>
                  <td>Khean</td>
                  <td>Male</td>
                  <td>20-20-2020</td>
                  <td>User</td>
                  <td>10-08-2020</td>
                  <td>01-08-2020</td>
                  <td style={{ textAlign: "center" }}>
                    <Button variant="warning" style={{ margin: "5px" }}>
                      Update
                    </Button>
                    <Button variant="danger" style={{ margin: "5px" }}>
                      Delete
                    </Button>
                  </td>
                </tr>
              </tbody>
            </Table>

            <nav aria-label="Page navigation example">
              <ul class="pagination">
                <li class="page-item">
                  <a class="page-link" href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                  </a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">
                    1
                  </a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">
                    2
                  </a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">
                    3
                  </a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </section>
      </div>
    );
  }
}
