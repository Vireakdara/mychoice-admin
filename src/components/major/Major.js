import React, { Component } from "react";

import { Table, Button } from "react-bootstrap";
import "./Major.css";

export default class Major extends Component {
  render() {
    return (
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h1 className="m-0 text-dark">Major</h1>
              </div>
            </div>
          </div>
        </div>

        <section className="content">
          <div className="container-fluid">
            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 btn-add-university">
              <Button variant="success">Add + </Button>
              <pre></pre>
            </div>
            <div className="col-xl-6 col-lg-6 col-md-6 col-sm-6 form-search">
              <form className="form-inline my-2 my-lg-0 float-right">
                <input
                  className="form-control mr-sm-2"
                  type="search"
                  placeholder="Search"
                  aria-label="Search"
                />
                <button
                  className="btn btn-outline-success my-2 my-sm-0"
                  type="submit"
                >
                  Search
                </button>
              </form>
            </div>

            <Table striped bordered hover>
              <thead>
                <tr>
                  <th className="th-style">ID</th>
                  <th className="th-style">Major Name</th>
                  <th className="th-style">Created At </th>
                  <th className="th-style">Updated At</th>
                  <th className="th-style">Action</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>Mark</td>
                  <td>Otto</td>
                  <td>Otto</td>
                  <td style={{ textAlign: "center" }}>
                    <Button variant="primary" style={{ margin: "5px" }}>
                      View
                    </Button>
                    <Button variant="warning" style={{ margin: "5px" }}>
                      Update
                    </Button>
                    <Button variant="danger" style={{ margin: "5px" }}>
                      Delete
                    </Button>
                  </td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Jacob</td>
                  <td>Thornton</td>
                  <td>Otto</td>
                  <td style={{ textAlign: "center" }}>
                    <Button variant="primary" style={{ margin: "5px" }}>
                      View
                    </Button>
                    <Button variant="warning" style={{ margin: "5px" }}>
                      Update
                    </Button>
                    <Button variant="danger" style={{ margin: "5px" }}>
                      Delete
                    </Button>
                  </td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Larry the Bird</td>
                  <td>Otto</td>
                  <td>Otto</td>
                  <td style={{ textAlign: "center" }}>
                    <Button variant="primary" style={{ margin: "5px" }}>
                      View
                    </Button>
                    <Button variant="warning" style={{ margin: "5px" }}>
                      Update
                    </Button>
                    <Button variant="danger" style={{ margin: "5px" }}>
                      Delete
                    </Button>
                  </td>
                </tr>
              </tbody>
            </Table>
            <nav aria-label="Page navigation example">
              <ul class="pagination">
                <li class="page-item">
                  <a class="page-link" href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                  </a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">
                    1
                  </a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">
                    2
                  </a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">
                    3
                  </a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </section>
      </div>
    );
  }
}
