import React, { Component } from "react";

export default class Dashboard extends Component {
  render() {
    return (
      <div className="content-wrapper">
        <div>
          {/* Content Header (Page header) */}
          <div className="content-header">
            <div className="container-fluid">
              <div className="row mb-2">
                <div className="col-sm-6">
                  <h1 className="m-0 text-dark">Dashboard</h1>
                </div>
                {/* /.col */}
                {/* <div className="col-sm-6">
                    <ol className="breadcrumb float-sm-right">
                      <li className="breadcrumb-item">
                        <a href="#">Home</a>
                      </li>
                      <li className="breadcrumb-item active">Dashboard v1</li>
                    </ol>
                  </div> */}
                {/* /.col */}
              </div>
              {/* /.row */}
            </div>
            {/* /.container-fluid */}
          </div>
        </div>

        <section className="content">
          <div className="container-fluid">
            {/* Small boxes (Stat box) */}
            <div className="row">
              <div className="col-lg-3 col-6">
                {/* small box */}
                <div className="small-box bg-info">
                  <div className="inner">
                    <h3>100</h3>
                    <p>Users</p>
                  </div>
                  <div className="icon">
                    <i className="fas fa-users" />
                  </div>
                  {/* <a href="#" className="small-box-footer">
                      More info <i className="fas fa-arrow-circle-right" />
                    </a> */}
                </div>
              </div>
              {/* ./col */}
              <div className="col-lg-3 col-6">
                {/* small box */}
                <div className="small-box bg-success">
                  <div className="inner">
                    <h3>
                      2<sup style={{ fontSize: 20 }}></sup>
                    </h3>
                    <p>User Share Experience</p>
                  </div>
                  <div className="icon">
                    <i className="fas fa-chart-line" />
                  </div>
                  {/* <a href="#" className="small-box-footer">
                      More info <i className="fas fa-arrow-circle-right" />
                    </a> */}
                </div>
              </div>
              {/* ./col */}
              <div className="col-lg-3 col-6">
                {/* small box */}
                <div className="small-box bg-warning">
                  <div className="inner">
                    <h3>10</h3>
                    <p>Training Schools</p>
                  </div>
                  <div className="icon">
                    <i className="ion ion-person-add" />
                  </div>
                  {/* <a href="#" className="small-box-footer">
                      More info <i className="fas fa-arrow-circle-right" />
                    </a> */}
                </div>
              </div>
              {/* ./col */}
              <div className="col-lg-3 col-6">
                {/* small box */}
                <div className="small-box bg-danger">
                  <div className="inner">
                    <h3>10</h3>
                    <p>Reporters</p>
                  </div>
                  <div className="icon">
                    <i className="ion ion-pie-graph" />
                  </div>
                  {/* <a href="#" className="small-box-footer">
                      More info <i className="fas fa-arrow-circle-right" />
                    </a> */}
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
